---
layout: post
title:  "Next Steps"
date:   2021-06-02 09:42:14 -0300
categories: jekyll update
---

<h1>Next Steps of the project</h1>

**Mechanical Part:**

1.	Test the flap model under different angles and compare its the quality of the produced smears with the clamp model. This would be expected to be similar to the clamp model's variations, but the extra stiffness may change the steeper angles results.

2.	Use smear+ to identify the ideal velocity. Here velocity could be varied much more accurately and tested for a large range. The effect of velocity on the measured force should also be measured with more accurate values.

3.	Examine the way velocity, force and angle are related using the smear+. This would give us a better of an idea how the force and angle variations are affected by using the better design as there will be even less human error, and so would need to be tested. The relationship between angle and force may also be different at these constant velocities.


**Image Processing Part:**


1. Find a way to quantify overlapping of cells (a significant overlapping was observed in smear images produced by the clamp model under 80 degrees and in smear images produced under high velocity, but this was not quantified).

2. Find a way to automate calculating cell distribution in each image (rather than segmenting the image into vertical and horizontal stripes and then separately analysing each stripe).

These five points are what we as a team believe the next steps should be in this project. We believe that Autohaem's next course of action is to investigate varying the speed of the device and carry out points 2 and 3 of the Mechanical section. When these experiments are carried out, the organisation will gain a much better idea of how to optimise the device further and can make any future experimentations based on the success of these experiments.

