---
layout: post
title:  "Force and Speed Analysis (Louis)"
date:   2021-06-05 07:42:14 -0300
categories: jekyll update
---
**Overview**

This post is on the topic of Force and speed Analysis that was carried out, how those test helped our overall objective, how were the tests implemented. Will also focus on what analysis still needs to be done e.g. with regards to speed

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Smears_.jpeg" height="320"></center>
<p align="center"> Figure 1: Procedure for performing a manual smear (current medical practice)</p>

**Why do we need to analysis the vertical force act on the bottom slide & other variables?**

The overall objective of our project is to “Make consistent, high quality automated blood smearing.” And to achieve such goal, we identified the most crucial variables in a smearing process as:
1.	the angle that the slide was inclined at.
2.	the force with which this is done.
3.	the speed that the smear is performed at.
For our design to success, we must investigate:
1.	possible connections between the three variables.
2.	the optimum parameters where possible.

**Objectives of the “Force Analysis”**

  1.	Find out how does top slides inclination angles and the speeds top slider move influence Initial Force, Final Force and Max Force (defined in the next section). Providing feasible hypothesis that may explain any observed relations between those parameters.
  2.	Provide evidence for investigating optimum smear parameters.


<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/IMG_4403.jpeg" height="250">
<img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Screenshot_2021-06-09_at_22.00.50.png" height="300"></center>
<p align="center">Figure 2: sliders with silde inclination angles from 40° to 80°, with 10° increment between each. | Figure 3: critical parameters in a smear procedure </p>


**How do we measure the vertical forces?**
1.	We first designed a bridge shaped rig which span cross the lab balance and provide a similar slider mounting to “Autohaem Smear” and made the rig by 3D printing.
2.	We fixed a lab balance onto an optical table (heavy metal surface with matrix of predrilled holes for the ease of clamp installation), taped the bottom slide to the balance’s weighing pan to avoid any movement.
3.	Mount sliders (with clamp mechanisms, with slide guide angled from horizontal at 40° to 80°, 10° increment from slider to slider) onto the rig.
4.	The balance readings reflect the instantaneous vertical force acts on the bottom slide.
5.	Conduct smearing using the slider per normal procedure, with human/horse blood substituted by water, the motions involved consists of a backward pull and a forward push, similar to the graph shown below.
6.	Conduct 9 smearing for slider with each angle (40° to 80°). In 3 smearing the slider will be pushed forward at “Fast Speed” (~5 cm/s), in another 3 smearing the slider will be pushed forward at “Normal Speed” (~3.3 cm/s), in the last 3 smearing the slider will be pushed forward at “Slow Speed” (~2.5 cm/s)
7.	Record the initial reading (initial force) before the smearing, when top slide naturally rests on the bottom slide without any applied motion.
8.	Record the final reading (final force) after the smearing, when top slide naturally rests on the bottom slide without any applied motion.
9.	Record the maximum reading (max force) during the smearing, when the top slide is pushed forward to spread the water surface.
10. As a placebo control group, we conduct the process (4 - 9) again by hand rather than using the slider (doing the smear at Normal Speed, ~45° slide inclination angle, to simulate the current smearing done by hand method).

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/IMG_4374.jpeg" height="300">
<img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Screenshot_2021-06-05_at_14.32.39.jpeg" height="300"></center>
<p align="center">Figure 4: Testing rig made by 3D printing and the optical table provides stable mount. | Figure 5: when the slider is mounted to the rig, smears (with water as smearing fluid) are done per normal procedure </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/IMG_4409.jpeg" height="350"></center>
<p align="center"> Figure 6: repeat the test by hand this time, compare readings with smear done using the slider.</p>

**Phenomenon we observed from testing data** 
  1.	The steeper the slide inclination angle, the smaller the initial vertical force act on the bottom slide.  (Such observation is counterintuitive since one would image at 90°, the vertical force would simply be the self-weight of top slide and would be the highest.)
<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Initial_Force__vs_Angle.png" height="350"></center>
<p align="center"> Figure 7: Slide Inclination angle vs Initial vertical force plot (each value taken from averages of 3 tests).</p>

  2.	Both the final force and the max force show clear minimum at about 70°, with higher vertical force act on the bottom slide for shallower or steeper inclination angles. (it’s also observed in our image analysis that 70° also has the highest numbers of cells in each image, most consistent spatial distributions of cells in each image, and the numbers of cells in each image see the least variations.)

  If 70° were to be the optimum inclination angle for our smearing set up, and our tests are representative of a real medical smearing. Then the current medical doctrine regarding making blood smears at 45° needs to be updated.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Final_Force_vs_Angle.png" height="350"></center>
<p align="center"> Figure 8: Slide Inclination angle vs Final vertical force plot (each value taken from averages of 3 tests), a minimum at 70° is clearly shown.</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Max_Force_vs_Angle.png" height="350"></center>
<p align="center"> Figure 9: Slide Inclination angle vs Maximum vertical force plot (each value taken from averages of 3 tests), a minimum at 70° is clearly shown.</p>

3.	Smear done by hand gave very similar Initial, Final, Max forces readings to smear done by our slider with 40° slide inclination angle.


**Our hypothesis about the phenomenon observed**
  1.	Variable 3. (the speed that the smear is performed at) and Variable 1. (the angle that the slide was inclined at) are mutually exclusive, or the impact one have on the other isn’t significant. 
  2.	There are strong connections between slide inclination angle and Initial, Final, Max forces. The connection between slide inclination angle and Final/Max forces are similar. (as described in the previous section)
  3.	Regarding the Phenomenon 1. described above, we don’t have a clear explanation. But further studies on more detailed force analyses for the system might answer why steeper slide inclination angles results in smaller initial vertical forces act on the bottom slide.
4. Regarding the Phenomenon 2. described above, from the experience in doing smears by hand, we think above 70° there is an increase in horizontal force Fx (see Figure 3) which causes a forward tipping moment to the top slide, in turn top pushes down harder, i.e., higher vertical force Fy  values.

**What could be done better?**

The points below discuss the issues we encountered in our test which well worth more research and analysis so improvements can be made for possible future project development. 

•	It’s difficult to judge the quality of the “mock smearing” when smearing fluid human blood is substituted with water (for Health & Safety reason), as:
    1.	water droplets on glass slides are hard to see, 
    2.	fluid characters (especially surface tension) of water made it hard to be spread out on a glass slide like human blood.
Therefore, we would recommend further studies on the autohaem project to use at least coloured liquid with higher viscosity than water.

•	The next step of autohaem project would involve quantifying variable 2. (the speed that the smear is performed at). Because in our “mock smearing” tests, the slider is pushed by hand, therefore, the distances which the slider moved each time are hardly consistent. Measuring such distance by pixel counting is both inaccurate and laborious, due to the only length reference — the bottom glass slide (76mm) has its both ends taped with non-transparent tape.

In further studies, if we continue using this force measuring method, we could improve the set up by fixing a ruler next to the bottom slide on the balance so we could have a more accurate way in knowing slider travelling distance.
Also deducing the time slider travels in each test run through counting video recording frames is also inaccurate and laborious, a better time measuring method will be necessary for reliable speed parameter calculation. 

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/IMG_4396.jpeg" height="320"></center>
<p align="center"> Figure 10: A better distance measuring solution with a ruler sit adjacent? It can be seen that the bottom slide (as length reference) has its edges covered by fixing tape</p>

•	The measuring pan on the balance where bottom slides sit isn’t stationary, but rather compliant with more room for vertical displacements further out from the centre (similar to a seesaw).  Such compliance may result in unreliable readings.  Further study can focus on improving force measurement solutions.
