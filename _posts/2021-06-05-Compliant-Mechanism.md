---
layout: post
title:  "Compliant Mechanisms (Duncan)"
date:   2021-06-06 8:43:14 -0300
categories: jekyll update
---

<h1> Objectives for the Mechanism </h1>

The first task of the project is to create a method to hold the glass slide in place. In order to keep track of our progress with this task, objectives were set so that the progress would be checked an we could aim for specific deliverables.

The mechanism needs to be able to:

1. Be easy to make and be compatible with Autohaem's design.

2. Hold the glass slide in place for the duration of the smear. This means that the glass slide will not jump and cause ridges in the smear, which needs to be avoided as it makes it much harder to analyse. 

3. Be able to be consistantly inserted into the same position, which will help improve the reliability and reproduceability of the smear device.

4. Produce a good smear reliably. This is the main aim of the project.

5. Be durable and robust. We set ourselves the target of not break for over 100 iterations of using the mechanisms.

Following the first criteria, it was decided that the would involve us using a 3D printed compliant mechanism. This was to build and improve on Autohaem's device in the easiest way. Autohaem were also already using OpenSCAD, so this was the modelling program that we would also go on to use.

**What is a Compliant Mechanism?**

A compliant mechanism is a mechanism that produces force and motion transmission through elastic body deformation. They are always flexible and use the motion of one part of the model in order to achieve their goal. They are paticularly useful when trying to limit the number of parts used in a project and make the assembly of the whole mechanism, if any is required, a much easier task. From this, it should be clear why one was useful to us. We were looking to make an easily constructable, cheap device with fewer moving parts.

<h1> 3D Printing </h1>

**How are the prototypes made?**

The prototypes are 3D printed in PLA. This is so that the clinics in Tanzania can print off their own models with relative ease and with minimal shipment of parts. 3D printing the models constrained what we could do in some respects, which are discussed later, but also allowed us to rapidly prototype the mechanisms that we came up with, allowing for a lot of improvement quickly, which was key in our short timeframe.

**3D Printing Limitations**

3D printing does have its limitations and these place some design constraints on the design. The constraints were wide ranging, from the shrinkage of the material to the compatability with compliant mechanism design. 

As we were printing in PLA, the models shrunk by about 2.5% when they cooled down from printing height. This is what caused a couple of issues with our prototypes and needed to be factored into our designs. However, the shrinkage was not consistant, which would make it easy to factor in, but rather it varied slightly with each print. This lack of a set tolerence makes the printing process a bit longer but overall could be worked around by including more tolerance in the designs.

Another issue with 3D printing is how to print compliant designs. It was found out the compliant devices need to have the compliant bend perpendicular to the 3D printed layers. This was an issue with the flap mechanism and was the reason why the model needed to be printed in two parts. Although this was fixable, it needed to be kept in mind for the duration of the project.

Printing orientation is a third common issue that faces 3D printed models. There cannot be any steep overhangs, as the plastic will not be abke to be printed here and bridging across large gaps was impossible. We were attempting to print without supports to make the models easier to prepare, which constrained the designs. This was workable around but constrained the design of the project further.

<h1> Design </h1>

Autohaem uses opensource software, so all the changes that are made are added to the GitLab and recorded. The designs are built on Autohaem's initial one, in a separate branch, before being merged back into the main one at the end of the project. The link to the final repository of models is <a href="https://gitlab.com/autohaem/autohaem-smear/-/tree/component-upgrade/openscad">here.</a>

Throughout the project, the code in the openSCAD had to be commented clearly in order to allow for the easiest of transitions back to Autohaem. What each section does can be clearly seen and edited if needed. There is also as many parameters made as possible. This allows a value to be changed, such as the model width, and the changes are automatically updated with the model. This helped us and Autohaem make changes to the mechanisms that were copied throughout the model automatically, rather than rising forgetting about a value or two that could break the rendering.

<h1> Products Developed </h1>

**What are the prototypes and how do they work?**

1)  The first prototype is a flap mechanism. The slider model that can be seen below includes a compliant flap that the glass slide pushes out of the way when inserted. The flap, while trying to return to its origional position, induces a force downwards on the glass slide, holding it in place using friction. This friction is a very hard quantity to quantify and further experimentation would be needed in order to determine its force. Due to the issues with 3D printing and compliance, the model needs to be printed in two parts, with nuts and bolts fastening them together (3mm diameter).

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Flap.png"> </center>
<p align="center">Figure 1: Flap mechanism</p>

2)  A second prototype involves a secondary 3D printed model that will be attached to the initial slider. This secondary model is a clamp and can be seen in the image provided. The clamp is the compliant part of the slider design in this case, with the forward and backward push pads controlling the distance that the guide pads are apart. The clamp is initially conncted to the slider using the connecting teeth, before pushing the push pads together in order to widen the gap between the guides. This creates enough space for the glass slide to be inserted until it is resting on the horizontal glass slide, with the push pads being released when the glass slide is in place, holding it here.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Clamp.png" align="center"></center>
<p align="center">Figure 2: Clamp mechanism</p>

**How do the mechanisms work?**
<center><video width="500" height="375" class="tab" controls>Your browser does not support the &lt;video&gt; tag.
<source src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Clamp_Autohaem.mp4"/>
</video></center>
<p align="center">Video 1: Video showing how to use the Clamp mechanism </p>
<center><video width="500" height="375" class="tab" controls>Your browser does not support the &lt;video&gt; tag.
<source src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/IMG_3195_Trim.mp4"/>
</video></center>
<p align="center">Video 2: Video showing how to use the Flap mechanism</p>
The video above shows how to use the mechanisms. Also in this section are individual photographs of each part with more detail.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Picture6.jpg" align="center"></center>
<p align="center">Figure 3: Half of a flap mechanism slide. The flap can be seen clearly in the slot, slightly obstructing where the glass slide will be in place. Nut holes can be seen as a fastening mechanism to fit the two halves together. New bolt hole area was added to this iteration, as can be seen in the model of the flap mechanism above, but is otherwise the final product. </p>

<center><img width = "350" height = "350" src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Picture7.jpg" align="center"></center>
<p align="center">Figure 4: Clamp part of the clamp mechanism. The guides can be clearly seen on the near and far part of the clamp, while the pushpads can be seen to be the left and right part of the model. The pushpads are also housing the clamp connecting teeth that will connect it to the slider. As the two pushpads are pushed closer together, the guides go further apart allowing the glass slide to be inserted. </p>

<center><img width = "700" height = "350" src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Picture8.jpg" align="center"></center>
<p align="center">Figure 5: Slider part of the clamp mechanism. The joins to hold the connecting teeth can be seen, but apart from that the model is very similar to the origional Autohaem design.</p>

**Rough Costing**

Both mechanisms as they stand are fairly cheap to produce. One of the main aim was to make the models cheap, so in doing this it is a big step towards sustainable development that we are aiming for. Both the mechanisms use the same base parts with the 3D printed slider the main difference, but have been itemised separately in the lists below. 

For the clamp mechanism, the list of parts is as follows:
- 3D Printed Slider 
- 3D Printed Clamp
- 3D Printed Main Body
- 2 Rods
- 2 Bearings

Flaps Parts:
- 3D Printed 2 Slider halves
- 3 sets of 3M nuts and bolts
- 3D Printed Main Body
- 2 Rods
- 2 Bearings

For both of these, the 3D printing parts are very cheap, costing about £2 for both. The rods and bearings also only come to ~£5, with the nuts and bolts also very cheap in the UK. Using these figures, the overall costing for each individual model is between £7 and £12, which is a massive improvement on the models that are currently available.

<h1> Testing and analysis </h1>

The mechanisms were tested with human blood drops and analysed underneath a microscope. The smears were carried out as seen in the videos above but in a steralised environment. The full analysis can be seen under the Image Processing post <a href="https://dfp24.gitlab.io/gm2-autohaem-malaria/jekyll/update/2021/06/05/Image-Processing">here.</a>

<h1> Review of Design </h1>
**Which prototype works better?**

Both these models have their advantages and disadvantages. For the Flap mechanism, it securely secures the slide in place much better, but this also causes the glass slide to be more difficult to get into position, especially an accurate one. On the other hand, the clamp is very easy to get in position and while it does not hold the glass slide as securely as the flap, it is still secure and only jumps when excessive force is applied.

Another point to consider is how easy they are to assemble and use in practice. Both mechanisms are printed in two separate parts, but while the clamp mechanism needs no extra parts and is fitted together via a compliant mechanism, the flap mechanism needs to be bolted together with 3 pairs of nuts and bolts. Since these models will be printed on location in Tanzania, having minimal extra parts is always a bonus, so this leans further towards the clamp mechanism being more suitable to the task.

With regards to the actual images that are produced, this is covered in the Image Processing page. In summary, both mechanisms had a similar value for the average number of cells, while they varied on how the cells were distributed. The flap mechanism tended to get a more consistant distribution, but due to the previous two points a clamp mechanism was still prefered.

**Criteria Assessment**

In deciding which prototype works better, it is best to look back at the criteria for our model. The mechanism needs to be able to:

1. Be easy to make and be compatible with Autohaem's design.

2. Hold the glass slide in place for the duration of the smear. This means that the glass slide will not jump and cause ridges in the smear, which needs to be avoided as it makes it much harder to analyse. 

3. Be able to be consistantly inserted into the same position, which will help improve the reliability and reproduceability of the smear device.

4. Produce a good smear reliably. This is the main aim of the project.

5. Be durable and robust. We set ourselves the target of not break for over 100 iterations of using the mechanisms.

From this the two can be easily compared. While both models fit most of the criteria, they both have their limitations. The clamp model fits points 1, 2, 3 and 4 very well, but work is needed to improve point 5. The flap model fits points 1, 2, 4 and 5 well, but point 3 still needs work, with the glass slide hard to get into the position. Without this, the model may be subject to undue forces and break more easily, but further analysis would need to be carried out in order to quantify this.

In all, the clamp mechanism is prefered due to its weakness being offset by how small and easy it takes to print another clamp (10 minutes). This offset cannot be said for the flap mechanism's issue, so the clamp is the prefered model.

**How do these improve the initial model?**

Both methods fixed the glass slide in place. This in itself was the main improvement to the smear mechanism and would improve the reliability of the smears. The main improvement from the origional design was the reproduceability of the smear performance, and as the mechanism now sets up most of the parameters for the user it removes a lot of the human error and improves this reproduceability. This has aspect is further explained in another page.

**What still needs to be done?**

One of the criteria that still needs to be tested is the last one, stating the robustness and durability of the compliant mechanism. This was not able to be tested properly and requires more analysis as to how durable the models are. They have successfully been used for 20+ iterations successfully on numerous occasions, but the models still break if not used correctly and are force, due to the nature of 3D printed compliant mechanisms.

The next steps involve optimising the mechanism. As the clamp mechanism is easier to use, and through the image processing is seen to give fairly similar results to the flap mechanism, it was decided that this was the model that we would be progresisng with. The optimisations that we were aiing to perform were the angle that the slide was inclined at, the speed that the smear is performed at and the force with which this is done. The angle and force optimisations were able to be carried out, but the speed one was not, and this will be the next step for the autohaem project.

The optimisation of the angle page can be found <a href="https://dfp24.gitlab.io/gm2-autohaem-malaria/jekyll/update/2021/06/05/angle-analysis">here </a> and the optimisation of the force and speed document can be found <a href="https://dfp24.gitlab.io/gm2-autohaem-malaria/jekyll/update/2021/06/05/Force-Analyis">here.</a>

**Where could the model be improved without further analysis?**

The model's main flaw at the moment is its variation in shrinkage. If another material can be used that is similar to PLA but does not shrink then that would be very usefull, but further research and experimentation would be required in order to discover this.

From a modelling point of view, the main flaw for the clamp version is its durability. The model and mechanism would benefit from a lot of finetuning, especially the clamp mechanism. The clamp works well at the moment if handled with care and it is not forced, but in a rushed environment it could be broken. It would need to be experimented with if there is a way to improve the durability without compromising the compliance then that would be idea. However we were not able to test this.


<a href="https://app.luminpdf.com/viewer/60c1d050839b2000195cbf57">Download Full Prototype log</a>
