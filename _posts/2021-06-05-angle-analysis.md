---
layout: post
title:  "Angle Analysis (Duncan + Anna-Katerina)"
date:   2021-06-05 09:42:14 -0300
categories: jekyll update
---

The first aspect that needed to be optimised was the angle of the inclined slide that is making this smear. This was because after some initial experiments, this was found to be a large indicator of how good a smear was. An initial angle of 40 degrees was chosen for convenience, but it was believed through manual testing that the angle made a big difference to the quality of smear produced. With force and speed of the slide held constant, we then varied the angle by 10 degrees from 40 degrees to 80 degrees.

**So why was no shallower angle considered?**

We decided to only test angles that would be applicable to actual use. Due to 3D printing constraints, where a overhang can only be 40 degrees at a minimum, any lower angle yhan this 40 degrees was not practical to print. This may have constrained our results slightly, but initial testing done by hand also showed that these angles did not produce good smears. An example of this can be seen below, where only one cell can be seen in a standard image. This can easily be compared with the good angle and high angle smears, whose images follow.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Low_Manual_testslide.png"> </center>
<p align="center">Figure 1: Test slide done at a low angle</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Good_angle.png"> </center>
<p align="center">Figure 2: Test slide done at a good angle</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/High_angle.png"> </center>
<p align="center">Figure 3: Test slide done at a high angle</p>


In contrast, the smears done by hand at a normal and at a high angle can be seen to be good smears. The high smears seem to have a denser amount of cells, so when we went on to more precisely vary the angle, we tested angles 40 degrees to 80 degrees inclusive to check if this was the case.

**Completed Models**

The final models, once the iterations had been completed are shown below. All five angles can be seen, with the glass slide clearly held in place for each one. These were then tested in the same manner that the previous smears were tested and the results analysed. All of these angles with the glass slides inserted can be seen in the figure below.

<center><img width = "450" height = "330" src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/IMG_4404.jpeg"> </center>
<p align="center">Figure 4: All five clamp mechanisms with varying angles</p>

Testing the glass slides was then carried out in an updated smear mechanism that Autohaem had improved while we were working on the designs. Some images of these different angles are shown below.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/40degrees.png">
<img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/50degrees.png"></center>
<p align="center">Figure 5:Test slide done at 50 degrees | Figure 6: Test slide done at 50 degrees</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/60degrees.png">
<img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/70_degrees.png"></center>
<p align="center">Figure 7: Test slide done at 60 degrees | Figure 8: Test slide done at 70 degrees</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/80degrees.png"></center>
<p align="center">Figure 9: Test slide done at 80 degrees</p>

**Initial Observations**

An initial observation from the images above, some initial trends can be seen. The main one here is that there is a clear increase in the density of the cells as the angle increases. With this increase in density, comes an increase in the overlap of the cells. Therefore it is logical that the ideal angle of smear will be situated at a high density, but before the overlap becomes a regular issue. This is where the image processing comes in.

<h2> Analysis (Anna-Katerina) </h2> 

<h1> Criteria for the quality of smear </h1>

To evaluate the quality of the smear the following criteria were taken into account: 
(a)	Average number of cells in the smear
(b)	Consistency of cell number in each smear
(c)	Cell distribution close to uniform one
(d)	Consistency of cell distribution in each smear

Cell number is important is important in order to more accurately identify percentage of parasites in infected red blood cells (parasitaemia). Consistency is important to obtain a reliable measurement. Uniform distribution makes easier to recognise individual red blood cells and thus identify parasites inside them.

<h1> Manually Produced Smears at different Angles </h1>

For the manually produced smears that can be seen in the first three images, it can be seen that the optimum angle should lie between a high angle and a medium angle. Since the aim was to identify a broad range of angles, smear were evaluated just based on cell number. The results are shown below:

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/B_W_highandmedium.png"> </center>
<p align="center">Figure 10: Box and Whiskers plot of cell number in images produced under good and high angles</p>

It can be seen that high angle smears have a significantly higher median value of cell number, but good angle smears are more consistent (max and min values-whiskers closer together). Cell number below that of the median value of good angle smear would produce a smear of low cell density and thus not suitable for analysis. Thus, it was concluded the ideal angle is between “good” and “high” angles, i.e. between 40 and 80 degrees. 

<h1> Smears produced using the clamp model at angles 40-80 degrees </h1>

Smears were produced using the clamp model under 40, 50, 60, 70 and 80 degrees, as can be seen in Figure 4. A smear was produced under each one of these angles and manually. 30 images were captured moving up the long axis of the smear every 200nm.

A smear produced manually was also constructed as it would normally be constructed under ideal lab conditions. 

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/SlideImaging.png"> </center>
<p align="center">Figure 11: Schematic representation of the way images were captured ([[5](clinicalgate.com/introduction-to-peripheral- blood-smear-examination/)]) </p>

**Cell Number** 

The smears produced under different angles are analyzed based on the criteria stated on (2). Cell number was evaluated by contracting a box and whiskers plot, shown in Figure 7.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/B_WAngles.png"> </center>
<p align="center">Figure 12: Box and Whiskers plot of cell number in images produced under varying angles and produced manually </p>

**Evaluation of Results**

It can be seen that 70 degrees is the angle that results in the highest average number of cells, which is also closer to the average number of cells produced by the manual smear. It is also observed that 70 degrees resulted in more consistent images (shorter whiskers in the box and whiskers plot).

This is a good indication for the quality of the smear produced under 70 degrees.

**Cell distribution** 

To evaluate cell distribution the proportion of cells in each field of view (image) was calculated and a box and whiskers plot was constructed, shown in Figure 13.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/B_W_distribution.png"> </center>
<p align="center">Figure 13: Box and Whiskers plot of percentage of total cell number in each image (field of view) produced under varying angles and produced manually </p>

**Evaluation of Results**

If the cells were uniformly distributed in each field of view, the proportion of cells in each image would be 3.33% (since there are 30 images). The average proportion of cells in image produced under almost all angles is close to this value. However, 70 degrees result in more consistent images in term of cell distribution (shorter whiskers). That is, cell distribution does not diverge a lot from uniform distribution. 

To further evaluate cell distribution, each image is divided every 250 pixels in the horizontal direction and eight columns are produced, and every 200 pixels in the vertical direction and six rows are produced, as shown in Figures 14 and 15. The number of cells in each column and row is calculated using CellPose. 20-250 pixel is around the size of three red blood cells and is consider a suitable size for the vertical and horizontal stripes.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Vertical.png" height="300" > </center>
<p align="center">Figure 14: Cellpose output image segmented every 250 pixels in the vertical direction </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Horizontal.png" height="300"> </center>
<p align="center">Figure 15: Cellpose output image segmented every 250 pixels in the horizontal direction </p>

The proportion of cells in each column and each row is then calculated and compared to the proportion of cells expected under uniform distribution (this would be 13% for the vertical stripes 1-7 and 9% for vertical stripe 8 since it is less than 250 pixels. For the horizontal stripes it would be 17% for stripe 1-5 and 16% for stripe 6). The squared difference from the expected proportion of cells is plotted for each angle. The resulting plots are shown in Figures 16 and 17.

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/B_Wfour.png"> </center>
<p align="center">Figure 16: Squared difference from uniform cell distribution in vertical stripes </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/B_Wfive.png"> </center>
<p align="center">Figure 17: Squared difference from uniform cell distribution in horizontal stripes </p>

**Evaluation of Results**

The lowest average squared difference is observed for 70 degrees in both the vertical and the horizontal stripes. This error is closer to the error observed in the manually produced smears. Thus, 70 degrees result in less variation from uniform distribution than any other angle. 

To further evaluate uniformity of cell distribution in smear, the coefficient of variation was also calculated. This is the standard deviation of the number of cells in stripe divided by the average number of cells in each field of view (1-30). This was calculated for each of images/fields of view and the average value was calculated for each of the test angles. 

For this calculation the vertical stripe 8 and the horizontal stripe 6 were not included since they are less than 250 and 200 pixels respectively (however, when these stripes are included the same qualitative results are observed). The resulting average coefficient of variation for each angle is shown below (Figures 18 and 19):

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/bar1.png"> </center>
<p align="center">Figure 18: Coefficient of variation of vertical stripes </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/bar2.png"> </center>
<p align="center">Figure 19: Coefficient of variation of horizontal stripes </p>

**Evaluation of Results**

A lower coefficient of variation means more consistent results in cell distribution in vertical and horizontal stripes for each image. From these two figures it can be further confirmed that smears produced at 70 degrees result in a more uniform cell distribution than those produced at other angles. 

<h1> 3D Printed Machine vs Manually Produced Smears </h1>

From all of the above figures, it is observed that the manually produced smear results in a higher average number of cells and a distribution closer to the uniform one. However, it has to be taken into account that this smear was produced under ideal lab conditions and the results would not be the same if it was produced in a developing country under time pressure conditions. The smear produced by the clamp model under 70 degrees preforms in many cases similarly to the manually produced smear. With further future improvements it could perform better and more consistently than smears produced manually. 


The full set of images produced under low, good and high angle and the CellPose output results can be found [here](https://drive.google.com/drive/u/1/folders/1yzc7ypKeo80VmIv17o158oE6MXdYFCYd).

The data and the analysis to obtain the graphs can be found [here](https://drive.google.com/drive/u/1/folders/1Tk_-yx8fD2YNSVr9l7PXVOfNwqcTPvZm).

The full set of images produced under 40-80 degrees using the clamp model, the manually produced smear used for comparison and the cellpose output results can be found [here](https://drive.google.com/drive/u/1/folders/1SuuU2jZHyHMq-ctT1Tfn6OMNCnyYN-Jn).

The data and the analysis to obtain the graphs can be found [here](https://drive.google.com/drive/u/1/folders/1k6ioZOCUHKzqo15oE1dNURqyDs39Y1Yg).

