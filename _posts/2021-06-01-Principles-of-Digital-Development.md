---
layout: post
title:  "Compliance with the Principles of Digital Development"
date:   2021-06-05 05:43:14 -0100
categories: jekyll update
---
<h2> UNICEF Principles of Digital Development </h2>


<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/principles_image.png"> </center>
<p align="center">Figure 1: Principles of Digital Development</p>

For this project digital technology is used to code in OpenSCAD the 3D printed model, to share, store and analyse data, and to analyse images using a suitable software. However, the principles of digital development are important guidelines for the success of any project and often can be generalized beyond the digital context.

<h1>1) Design with the User </h1>

A key part of the project is to understand the challenges and needs of clinicians performing the smearing in developing countries. The main challenge is the difficulty to produce a high-quality smear, especially under time-pressure conditions. This creates the need for a way to produce smears of high-quality reliable and consistently, in order to aid diagnosis of malaria. 

In our case, we discussed with Samuel who has performed many smears and is in a position to describe the main challenges faced. We also had the perspective of Anna-Katerina, a medical student. 

However, we did not discuss with doctors in developing countries but once the 3D printed model is in its final form, local doctors will use it and give feedback. It is very important to see if they are willing to adopt with new way of producing smears and discuss with them possible changes needed. 


<h1>2) Understand the Existing Ecosystem </h1>

A main priority was to understand the context in which the device will be used. We aimed to use cheap, sustainable and robust materials, make the model easy to construct and to use. In this way, it is more likely to be used by local doctors. 


<h1>3) Design for Scale </h1>

It is also important to consider if the model will be widespread adopted for smear production. Hospitals and labs need to have a number of these 3D printed devices and be able to replace them in case of damage.  For this reason, in the initial prototype given to us and any improvements we added flexibility of model was taken onto account, i.e. being able to easily modify the code to make any adjustment needed (based on testing and feedback received) and print a new 3D printed model.


<h1>4) Build for Sustainability </h1>

The project has the potential for sustainability since it is very likely to be embedded into local guidelines for smear production, if it is proven to work effectively. Eradication of malaria is one of WHO’s top priorities and diagnosis of the disease is a key part to achieve that. Taking into account the low-quality of smears currently produced in the developing countries, funding and efforts to improve this 3D printed model are more than likely to be continued.  

<h1>5) Be Data Driven </h1>

In every part of the project decisions were driven by data available to us, data from previous research on the topic and data produced by testing the 3D printed model and analysing produced smears with aim to improve the research on optimising the device. Specifically, the results obtained by each analysis step were always evaluated and, thus, future directions were defined. Moreover, the way all the data are produced throughout the project is clearly explained and data are made available (code, excel documents, images). In this way, we inform the next steps of the project and make it easy for someone to continue working on that.

<h1>6) Use Open Standards, Open Data, Open Source and Open Innovation </h1>

For the coding of the 3D printed model, an already existing open platform, GitLab, was used to make sharing and modification of code easy and open source. We also used open data since all the data of the project are freely available. We felt that this is key to general collaboration, with anyone able to download the models for themselves and use them for free.

<h1>7) Reuse and Improve </h1>

At every part of the project, we evaluated the software and tools available to us and aimed to make as an effective use as possible in order to achieve the goals of the project. For example, we started off by studying what compliant mechanisms are and where are currently used in order to come up with an idea to keep the glass slide in place. For the image processing part, we examined softwares already available for cell identification and try to define the most suitable one for our purposes. 

Moreover, at every step we tested the existing protype and improve it, which a process that will continue to happen by the Autoheam to achieve a satisfactory result. 

<h1>8) Address Privacy and Security </h1>

For this project we did not use personal data and patient data are not likely to be used in the future work of the project. Smear and Smear+ will be used by appropriately trained personnel and then the diagnosis will be done under a microscope by a clinician as it would normally be done. Thus, there is no need to store and process patient data.

<h1>9) Be Collaborative </h1>

Throughout the project we were in constant communication with each other and with Pietro and Samuel to better define the goals Autoheam aimed to achieve. We quickly realized that in order to better progress with the design and analysis group discussions and collaboration were key. We created documents in google drive and opensource platforms so that all of us could modify and improve. Moreover, we understood the significant place this project has for sustainable development goals and WHO aims. At next steps Autoheam is likely to contact such organizations to discuss and get support to achieve faster, accurate diagnosis of malaria in developing countries. Another aspect that aided collaboration was that all of the work was also done on opensource software and is available to anyone for free, linking to the sixth point.

