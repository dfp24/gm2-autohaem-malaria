---
layout: post
title:  "Image Processing (Anna-Katerina)"
date:   2021-06-05 05:43:14 -0100
categories: jekyll update
---

<h1> I. INTRODUCTION </h1>

A 3D printed model can be used to automate smear production. Details of the 3D model are given in the compliant mechanism page, here. The overall aim of the project is to improve an existing 3D model prototype and test if the improved model results in high-quality smears and, thus, if it has the potential to indeed aid in diagnosis of malaria (since identification of parasites inside red blood cells will be easier with a high-quality blood smear).

To achieve this aim the following sub-tasks were defined: 

1.	Choose the most suitable image processing software to identify and count red blood cells in smear.
2.	Identify the criteria for a good quality smear.
3.	Compare the quality of smears produced using two different mechanisms.
4.	Identify the ideal angle to produce a high-quality smear using the 3D printed model – compare this smear with manually produced smear.
5.	Identify a range for the ideal velocity of the slide used as a blade.

The main parameters that could affect the quality of the smear ar angle under which the smear is produced, force applied on the horizontal glass slide and velocity at which the glass slide that acts as a blade is moving to perform the smear. These parameters need to be optimized so that the highest quality smear is produced by the 3D model. In this project we focused mainly on optimising the angle. We started examining the effect of velocity on smears but further work is needed on this part.


<h1> II. TECHNICAL WORK </h1>

<h3> 1. Image Processing Software </h3>


The aim of this task is to identify the best image processing software available to identify red blood cells in a smear. Four different image processing softwares and tools were compared to identify the one with the best performance. Specifically, eleven sample images of human red blood cells were analysed using (a) CellProfiler, (b) Hough Transform in FIJI, (c) Weka Segmentation Tool in FIJI and (d) CellPose.

To characterize the performance of each software, the output of each was compared to the original image. In this way the number of cells correctly identified (true positives), the number of cells not identified (false negatives) and the number of cells wrongly identified (false positives) were calculated. Based on these measurements the precision and the sensitivity were also calculated (this was done  by Duncan Parry and Louis Zeng)

Precision shows how many of the cells identified are actually present in the picture and is defined as follows:

Precision = True positives / (True positives + False positives)

Sensitivity shows how many of the cells actually present in the picture are identified and is calculated as follows:

Sensitivity = True positives / (True positives + False negatives)

The results are shown in Figure 1. 

| Software         | Precision  | Sensitivity  |
| ------           | ------     | ------       |
| CellProfiler     | 0.934      | 0.844        | 
| Weka             | 0.988      | 0.917        | 
|Hough Tranform    | 0.970      | 0.948        |
|Cellpose          | 0.995      | 0.983        |


Figure 1: Precision and Sensitivity of software

It can be seen that CellPose is the software with the highest sensitivity and precision. Thus, CellPose was used for the subsequent choice of mechanism and ideal angle identification.

Details about each software can be found below: 



<details><summary>CellProfiler</summary>

<h3>CellProfiler </h3>

<p>CellProfiler is an open-source software most commonly used to identify objects such as cells. Individual commands can be placed in order to form a pipeline. </p>
<p>For the red blood cells identification task the command used are: </p>
<p>1.	Rescale intensity between 0.5 and 0.7 </p>
<p>2.	Enhance edges </p>
<p>3.	Identify objects in diameter range 70-100 pixels </p>
<p>4.	Extract spreadsheet </p>

<p>An example of the output of this software is shown in Figure 2:</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/CellProfiler_image.png" height="300"> </center>
<p align="center">Figure 2: Example of output image of CellProfiler</p>
</details>


<details><summary>Hough Circle Transform</summary>

<h3>Hough Circle Transform </h3>

<p>The Hough Circle Transform is an image transform available as a plug-in in FIJI (ImageJ) that allows identification of circular objects.</p>

<p>The transform identifies circles based on the rotational symmetry of the perimeter. Thus, the data need to be converted to this form. To achieve that, a number of commands are run before the Hough transform is applied:</p>
<p>1.	8-bit: converts the image in grayscale</p>
<p>2.	Find Edges: the edges of the objects are identified and enhanced. Only the edges are preserved</p>
<p>3.	Thresholding: Objects above a certain threshold are painted black and below it are painted white. By selecting “dark background” the perimeter of the cells is white while the background is black.</p>

<p>Hough Transform: Cell diameter parameters tuned such that objects with minimum radius of 40 pixels and maximum radius of 45 pixels are identified. The Hough score threshold sets the minimum cut-off for the Hough score (i.e. ratio of votes) that a circle can have to count as a valid object. In this task the score is set between 0.3 and 0.45. </p>

<p>The centroids are marked on the original image on the output as shown in Figure 3: As the output one can also get measurements in the results table.</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Hough_transorm_image.png" height="300"> </center>
<p align="center">Figure 3: Example of output image of FIJI Hough Transform </p>

<h3>Hough Score</h3>

<p>It is interesting to examine the way the Hough score affects the performance of the Hough transform tool. To do so, two images, one of high cell density and one of low cell density, are analysed using a score from 0 to 1. Specifically, the values 0.0, 0.1, 0.2, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0 were examined and the precision and sensitivity of the transform under these scores was calculated.  </p>

<p>The original two images are shown in Figures 4 and 5. </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Low_cell_density_image.png" height="300"> </center>
<p align="center">Figure 4: Image with low cell density </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/High_cell_density_image.png" height="300"> </center>
<p align="center">Figure 5: Image with high cell density </p>

<p>The graphs showing the way precision and sensitivity vary with Hough score are shown below (Figures 6 and 7).</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Hough_score_and_low_cell_density.png""> </center>
<p align="center">Figure 6: Effect of Hough score on precision and sensitivity in low cell density image </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Hough_score_and_high_cell_density.png"> </center>
<p align="center">Figure 7: Effect of Hough score on precision and sensitivity in high cell density image  </p>


<h3> Evaluation of results </h3>

<p>As Hough score increases, imperfect of incomplete circles are less easily identified. Thus, the number of false positives is decreased but the number of false negatives (cells not identified) increases. For this reason, precision and sensitivity follow oppositive trends. As Hough score increases sensitivity decreases since more and more red blood cells are not identified but precision increases since the cells that are identified are indeed present in the original image (are not false positives).</p>

<p>It is observed that the Hough score that results in the highest precision and sensitivity (the better trade-off between the two) is between 0.38 and 0.39 for low cell density and between 0.39 and 0.40 for high cells density. If we take the mean value of 0.385 and 0.395 the result is 0.39. Thus, if the Hough score parameter is set at a value around 0.39 the result will be of good precision and sensitivity for images of both low and high cells density.</p>

</details>


<details><summary>Weka Segmentation tool</summary>

<h3> Weka Segmentation tool </h3>

<p>Weka Segmentation tool is a FIJI plugin that uses machine learning algorithms to classify objects into different classes. It uses a random forest classifier. This tool can be trained based on just one image and then the trained classifier can be saved and applied to other images.</p>

<p>In this task two different classifiers were used, trained on two different sample images. An example of the output of the classifier is shown in Figure 8:</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Weka_image.png" height="300"> </center>
<p align="center">Figure 8: Example of output image of FIJI Weka segmentation tool  </p>

<p>After that the classified image is further analyzed using the following commands in FIJI:</p>
<p>1.	8-bit: the image is converted to grayscale</p>
<p>2.	Find Edges: the edges of the objects are identified and enhanced. Only the edges are preserved.</p>
<p>4.	Thresholding: Objects above a certain threshold are painted black and below it are painted white. In this case “dark background” in not selected and thus the perimeter of the cells is black and the background is white.</p>
<p>5.	Analyse particles. Objects above a certain size are identified. The range “400- infinity” is selected in this case.</p>

<p>An example of the output of the “analyse particles” command is shown in Figure 9:</p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Analyse_particles_output.png" height="300"> </center>
<p align="center">Figure 9: Example of output image of FIJI “analyse particles” function with classified image 8 as input  </p>

<h3> Classifier </h3>

<p>The performance can be further improved by individually training each image, but this is very time consuming. For example, the image in Figure 10 is classified based on a classifier trained on another image and based on a classifier trained on the image in Figure 10 itself. The result is shown in Figure 10 and 11.</p>


<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Original_Weka_image.png" height="300"> </center>
<p align="center">Figure 10: Original image </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Output_image_of_FIJI_Weka_segmentation_tool_when_a_classifier_trained_on_another_image_is_used.png" height="300"> </center>
<p align="center">Figure 11: Output image of FIJI Weka segmentation tool when a classifier trained on another image is used  </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Output_image_of_FIJI_Weka_segmentation_tool_when_a_classifier_trained_on_the_original_image_itself_is_used.png" height="300"> </center>
<p align="center">Figure 12: Output image of FIJI Weka segmentation tool when a classifier trained on the original image itself is used  </p>

<p>The maps of identified objects (output of “analyse particle” command) for Figure 11 and Figure 12 are shown below: </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Example_of_output_image_of_FIJI__analyse_particles__function_with_classified_image_in_figure_9_as_input.png" height="300"> </center>
<p align="center">Figure 13: Output image of FIJI “analyse particles” function with classified image in figure 11 as input </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Example_of_output_image_of_FIJI__analyse_particles__function_with_classified_image_in_figure_10_as_input.png" height="300"> </center>
<p align="center">Figure 14: Output image of FIJI “analyse particles” function with classified image in figure 12 as input </p>

<p>Precision and sensitivity were calculated for each image. The results are shown in Figure 15. </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Effect_of_classifier_on_precision_and_sensitivity_.png"> </center>
<p align="center">Figure 15: Effect of classifier on precision and sensitivity  </p>

<h3> Evaluation of the results </h3>

<p>It can be seen that when the classifier is trained on the image itself precision is slightly reduced but sensitivity increases significantly. Sensitivity increases because cells are more clearly separated and thus a higher number of cells present in the image can be identified by the FIJI “analyse particles” command. Precision is reduced because objects that are not actually cells are identified as cells and thus the number of false positives increases. These objects are usually smaller than red blood cells and this issue is likely to be resolved if the size above which objects are identified is further increased in the “analyse particles” command.</p>


</details>


<details><summary>Cellpose</summary>

<h3>Cellpose</h3>

<p>Cellpose is a deep learning-based segmentation method. Cellpose was trained on a dataset of highly varied images of cells, containing over 70,000 segmented objects.It does not require training or parameter adjustments. For this reason, it is significantly quicker than all of the other software. The only settings that can be tuned are: cell diameter, model match threshold and cell probability threshold. For this task, cell diameter was set between 70-100 pixels (based on images), cell probability threshold was set to the middle and model match threshold was set to the second maximum position.</p>
<p>The output of CellPose can be obtained using the code found [here](https://drive.google.com/drive/u/1/folders/12gI25JWYGDbpr9UCXdjVuZzh3XB_L-h4) ([[6](https://cellpose.readthedocs.io/en/latest/outputs.html )]). An image with the masks of objects identified as cells can be produced as shown on Figure 16 and an image with red outlines of identified cells overlaid on the original image is shown in Figure 17. The axis in all the output images in cellpose are in pixels. </p>

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Cellpose_masks_image.png" height="300"> </center>
<p align="center">Figure 16: Example of mask output image of Cellpose   </p>



<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Cellpose_outlines_image.png" height="300"> </center>
<p align="center">Figure 17: Example of outline output image of Cellpose    </p>


</details>




<p>The full set of sample images used, and the output results of each software can be found ( <a href="https://drive.google.com/drive/u/1/folders/1abQIO_kCFtYU-Rx2fYHwzBZA2LwG8ecE"> here </a>)


<p>The data and the analysis to obtain the precision and sensitivity can be found ( <a href="https://docs.google.com/spreadsheets/d/1BTTlEcPsBMOmvRRXc09nYKF6gBfdd3rf/edit#gid=1960509594"> here </a>)



<h3> 2. Criteria for the quality of smear </h3>

To evaluate the quality of the smear the following criteria were taken into account: 
(a)	Average number of cells in the smear
(b)	Consistency of cell number in each smear
(c)	Cell distribution close to uniform one
(d)	Consistency of cell distribution in each smear

Cell number is important is important in order to more accurately identify percentage of parasites in infected red blood cells (parasitaemia). Consistency is important to obtain a reliable measurement. Uniform distribution makes easier to recognise individual red blood cells and thus identify parasites inside them.




<h3> 3.	Different mechanisms for smear analysis: Clamp vs Flap </h3>

In the project two different mechanisms to keep the glass slide in place were developed. A mechanism that keeps the slide in place by a stiff flap and a compliant clamp. A smear was produced using these two mechanisms under 40 degrees. 20 random fields of view of this smear were captured (Figure 3) and the resulting images were analysed.



<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Images_captured_.png"> </center>
<p align="center">Figure 3: Schematic representation of the way images were captured </p>



To evaluate cell number a box and whisker plot is produced for each mechanism, as shown in Figure 4.



<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Cell_number_Clamp_vs_Flap.png"> </center>
<p align="center">Figure 4: Box and Whiskers plot of cell number in images produced by the clamp and flap mechanism </p>



To evaluate cell distribution the proportion of cells in each field of view was calculated and a histogram and a box of whisker plot was constructed (Figures 5 and 6).



<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Cell_proportion_Clmap_vs_Flap.png"> </center>
<p align="center">Figure 5: Histogram of percentage of total cell number in each image (field of view) produced by the Clamp and Flap mechanism </p>



<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Proportion_of_cells_Clmap_vs_Flap_2.png"> </center>
<p align="center">Figure 6: Box and Whiskers plot of percentage of total cell number in each image (field of view) produced by the Clamp and Flap mechanism </p>



<h3> Evaluation of the results </h3>

From the figures it can be concluded that the Clamp model results in:
(a)	a similar average number of cells with the Flap model
(b)	similarly distributed smears in comparison to the Flap model
(c)	less consistent results in cell number and cell distribution

<p> From this analysis it was not clear which model to use for identifying the ideal angle and for comparison with the manually produced smear. It seems that the Flap model would be a better model to use but the Clamp model was much easier to use. Thus the Clamp mechanism was chosen and used for angle analysis. </p>



<p> The full set of images produced by the Clamp and Flap model and the cellpose output results can be found <a href="https://drive.google.com/drive/u/1/folders/1ulmKe_A0z5m7Gs3ViWPsF7q82GH6Jimu"> here </a>. </p>



<p> The data and the analysis to obtain the graphs can be found <a href="https://drive.google.com/drive/u/1/folders/1fJv4z_uhhW-wW7TzYCmPZE5FPeOSpzOC"> here </a>. </p>



<p> <h3> 4. Ideal Angle </h3> </p>

<a href="https://dfp24.gitlab.io/gm2-autohaem-malaria/jekyll/update/2021/06/05/angle-analysis"> Link here to Angle Analysis page </a>





<p> <h3> 5. Ideal Velocity Range </h3> </p>

**Smear produced using the Clamp model under different speeds**

In a similar way as the angle analysis, in order to identify the ideal speed, smears are produced under low and high speed. Ten images were captured and analyzed from each slide. The aim is to identify a range of angles that would be further tested using the Clamp model. An image for each set is shown below (Figures 7 and 8): 

<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/High_velocity.png" height="300" > </center>
<p align="center">Figure 7: High velocity image </p>



<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Low_velocity.png" height="300" > </center>
<p align="center">Figure 8: Low velocity image </p>



The smear was evaluated just based on cell number and further testing using smear+ to control the speed accurately is required.
The results are shown in Figure 9.


<center><img src="https://gitlab.com/dfp24/gm2-autohaem-malaria/-/raw/master/_images/Cell_number_-velocity.png"> </center>
<p align="center">Figure 9: Box and Whiskers plot of cell number in images produced under low and high velocity </p>


<h3> Evaluation of Results </h3>

It can be seen that higher velocity results in a very high average number of cells and low velocity in a very low number of cells. From Figure 37 it can be seen that high velocity results in overlapping of cells since the number of cells in each image is very high. None of these velocities produces a smear with good quality, therefore a velocity between high and low would result in a better-quality smear. To identify the ideal velocity a motor would be needed to accurately measure the velocity and then image processing analysis of the resulting images in the same way as for the angles 40-80. In the time-frame of the project this could not be done but velocity is an important parameter that needs to be evaluated in the future.


<p> The full set of images produced under low and high velocity can be found <a href="https://drive.google.com/drive/u/1/folders/1t1Iw4gQtPbc7kFwz5exJCSvMA8bOYqKD"> here </a> </p>

<p> The data and the analysis to obtain the graphs can be found <a href="https://drive.google.com/drive/u/1/folders/19CSm3HyJO6RDYY2l7Xyyy-iUE7VEZqOl"> here </a> </p>






<h1> III. CONCLUSION </h1>

<h3> 1.	Remarks </h3>

It can be concluded that the defined sub-tasks presented at the beginning of the project were successfully performed. The main results obtained are:

- Cellpose was chosen as the most suitable software since it results in higher precision and sensitivity. 
- The Clamp model was chosen to perform angle analysis mainly because of its greater easy of use. 
- The ideal angle was found to be 70 degrees (when using the Clamp model).


It is important to note that the comparison of the smears produced by the 3D printed model and produced manually gives very promising results. It shows that, with further improvements, automated blood smear production can indeed result in high quality smears and thus allow a more accurately diagnosis of malaria.

<h3> 2. Improvements </h3>


Possible improvements include:

- Use a higher number of images to identify the best image processing software and tools for red blood cells identification.

- Further examine which is the ideal number of bins to split the images in the horizontal and vertical directions. For example, Sturge’s Rule may  be useful which gives the following formula:

   K = 1 + 3. 322 logN

   where:
   K = number of class intervals (bins).
   N = number of observations in the set.
   log = logarithm of the number

   Since in this case there are around 200 cells per image the number of intervals used would be 9.



