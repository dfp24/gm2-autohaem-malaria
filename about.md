---
layout: page
title: Project Management
permalink: /about/
---

<h1> The Team </h1>

The team for this project consisted of three third-year engineering students:
- Duncan Parry, a Aero/Mechanical engineer 
- Louis Lee, a Civil engineer
- Anna-Katerina Leledaki, a medic studying Bio/Info engineering

This gave us a diverse group with which to distribute work. Due to COVID, Anna-Katerina was not situated in Cambridge, so it was decided that she will work on the image processing part of the project. This was more doable with remote working and coincided well with her Information engineering studies. Louis and Duncan were both situated in Cambridge, so it was more practical for us to work directly with Autohaem in person. Duncan worked on the main design of the mechanisms, working mainly with openSCAD and the 3D printers. Louis was focussed on the optimisation of the devices, focussing mainly on how force and speed affected the quality of the smears.

<h1> How we worked </h1>

While we all had the individual main tasks, we kept ourselves flexible due to the small nature of the group. Everyone contributed to each one of the three parts to some amount. This allowed us to minimise dead time and really helped with progressing the project and making sure that we had an output for all the main sections of the project.
We had regular meetings every one or two days to disuss and resolve isues arising and infrom other members of the team about the progress doen individually. At these meetings we were also revisiting our future steps, with the aim of the project always on mind. This helped to stay coordinated and motivated to perform at our best. 

An example of our flexibility when working as a group is to how we split our time. At the start of the project, there were no images that had been produced by the mechanical team, so Anna-Katerina assisted with compliant design brainstorming for the first week. Another example is towards the end of the project when the models had been produced, Duncan assisted Louis with the Force and Speed analysis.

<h1> Adaptability </h1>
For the duration of the project, our aims and objectives did not stay constant. We initially were just going to focus on the part of the projct with a compliant mechanism but quickly realised that other factors, like the angle, force and speed of smear were going to have a big effect on our project so we incorporated them in to the project. The image processing goals also changed. At the start, we were focussed on finding the percentage of image that was taken up by cells, using the WEKA segmentation or another software. However as we discovered new softwares we realised we would have to change how we approached the task. This adaptability was key for us to make more progress on the project, by focussing on what we could do and what we couldn't.

<h1> Issues </h1>

As mentioned above, we couldn't do everything. We struggled to find a way of objectively quantifying what exactly constituted a good and bad smear, with overlapping cells paticularly tricky. Therefore we relied solely on distribution and number of cells to make the quantifying. A second point of an issue was the speed analysis. As the motot was not available to use, we had to attempt to do this by hand, which greatly hindered the accuracy of the different controlled speeds. This is something that will need further experimentation, so we decided to narrow it down to mainly focus on the force analysis.

<h1> Success of the project </h1>

As an overall project, we have considered the project a big success. We, as a team, have successfully managed to come up with a mechanism to fix the glass slide in place and start optimising the various parameters that affect the smear. In coming up with an image processing method, we have also found a way to quantify how good a smear is. There need to be improvements, as was always likely with a four week project, but we have helped progress the project as we would have hoped for.

